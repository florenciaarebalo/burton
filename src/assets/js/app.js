import $ from 'jquery';
require ('webpack-jquery-ui/sortable');
import 'bootstrap';
import 'bootstrap-slider';

import "../scss/app.scss";
import { size } from 'lodash';

(function() {
  var requestAnimationFrame = window.requestAnimationFrame 
    || window.mozRequestAnimationFrame 
    || window.webkitRequestAnimationFrame 
    || window.msRequestAnimationFrame;

  window.requestAnimationFrame = requestAnimationFrame;
})();

$(function() { 

  //#region Cargar video
  
  let $video = $("#video");
  let video = $video.get(0);
  let currentTime = 0;
  let duration = video.duration;

  //#endregion

  //#region Representar Video

  let $canvas = $("#preview");
  let canvas = $canvas.get(0);
  canvas.width = video.width;
  canvas.height = video.height;
  let context = canvas.getContext('2d');
  
  //#endregion 

  //#region Tracks
  
  let $tracksContent = $("#tracks-content");
  let storage = window.localStorage;
  let data = storage.getItem('data');
  data = JSON.parse(data);

  if (data === null) {
    data = [];
  }

  let elementDefaults = {
    text: {
      type: "text",
      name: "Track",
      x: 0,
      y: 0,
      width: 0,
      height: 0,
      font: "Dancing Script",
      size: 10,
      color: "#000000",
      text: "",
      start: 0,
      active: true,
    },
    image: {
      type: "image",
      x: 0,
      y: 0,
      width: 100,
      height: 100,
      image: 0,
      start: 0,
      active: true,
    }
  };

  $(data).each(function (key, value) {
    trackAdd(value);
  });

  //#endregion

  //#region Propiedades

  let $properties= $("#properties");
  let $propertiesForm = $('#properties-form');
  let $propertiesSaveBtn = $("#properties-save-btn");
  let $videoSaveBtn = $("#video-save-btn");

  //#endregion

  //#region Controles
  
  let $playBtn = $("#play-btn");
  let $addTextBtn = $("#text-add-btn");
  let $addImageBtn = $("#image-add-btn");
  
  let $slideMaster = $("#slider-master");
  $slideMaster.slider({
    min: 0,
    max: duration,
    step: 0.1,
    precision: 6,
    value: 0,
    tooltip: 'hide'
  });

  //#endregion

  //#region Eventos

  $playBtn.on("click", function (e) {

    if (video.paused) {
      video.play();
      drawLoop();
    }
    else {
      video.pause();
    }

  });


  $addTextBtn.on('click', function () {
    elementAdd('text');
  });

  $addImageBtn.on('click', function () {
    elementAdd('image');
  });

  $propertiesSaveBtn.on('click', function () {
    elementSave();
  });

  $videoSaveBtn.on('click', function () {
    videoSave();
  });

  video.addEventListener('play', function () {
    $("#play-btn").html("<i class=\"fas fa-pause\"></i>");
  });

  video.addEventListener('pause', function () {
    $("#play-btn").html("<i class=\"fas fa-play\"></i>");
  });

  video.addEventListener('ended', function () {
    video.currentTime = 0;
  });

  video.addEventListener('seeked', function () {
    draw();
  });

  $slideMaster.on('change', function(e) {
    video.currentTime = $slideMaster.slider('getValue');
  });

  //#endregion
  
  //#region Funciones 

  function trackAdd (element) {

    // Contenedor principal
    let $container = $("<div>");
    $container.appendTo($tracksContent);
    $container.attr("id", "track-" + element.id);
    $container.addClass("track");

    // Etiqueta
    let $label = $("<div>");
    $label.appendTo($container);
    $label.addClass("track-label");

    let $text = $("<div>");
    $text.appendTo($label);
    $text.addClass("text");
    $text.html(element.name);

    // Botones
    let $buttons = $("<div>");
    $buttons.appendTo($label);
    $buttons.addClass("buttons");

    let $editBtn = $("<button>");
    $editBtn.appendTo($buttons);
    $editBtn.addClass("btn btn-sm btn-dark edit-btn");
    $editBtn.html("<i class=\"fas fa-pen\"></i>");

    $editBtn.on("click", function (e) {
      e.preventDefault();

      // Seleccionar el track
      $tracksContent.find(".track").removeClass('selected');
        
      $container.addClass('selected');

      // Mostrar formilario de propiedades del track
      $properties.addClass('selected');
      
      // Añadir valores de track a los campos de formulario de propiedades
      $properties.find(".field").hide();
      $properties.find(".field-type-" + element.type).show();

      $.each(element, function (key, value) {
        $("#property_" + key).val(value);
      })

      if (element.type === 'image') {
        ("#image-" + element.image).click();
      }
    });

    let $deleteBtn = $("<button>");
    $deleteBtn.appendTo($buttons);
    $deleteBtn.addClass("btn btn-sm btn-dark delete-btn");
    $deleteBtn.html("<i class=\"fas fa-trash\"></i>");

    $deleteBtn.on('click', function () {
      elementDelete(element);
      $container.remove();
      $properties.removeClass("selected");
      form.reset();
    });

    let $hideBtn = $("<button>");
    $hideBtn.appendTo($buttons);
    $hideBtn.addClass("btn btn-sm btn-dark eyeslash-btn");
    $hideBtn.html("<i class=\"fas fa-eye-slash\"></i>");

    // Timeline
    let $timeline = $("<div>");
    $timeline.appendTo($container);
    $timeline.addClass("track-timeline");

    let $slider = $("<input>");
    $slider.appendTo($timeline);
    $slider.attr("name", "track-" + element.id);
    $slider.attr("type", 'text');

    $slider.slider({
      min: 0,
      max: duration,
      range: true,
      step: 0.1,
      precision: 6,
      value: [element.start, element.end],
      tooltip: "hide"
    });

    $slider.on("change", function() {
      let values = $slider.slider("getValue");
      element.start = values[0];
      element.end =  values[1];
    });
  }

  function drawLoop () {
    requestAnimationFrame(drawLoop);
    
    if (video.ended || video.paused) {
      return;
    }

    draw();
  }

  /**
   * Represeta el video con sus elementos dentro del canvas
   */
  function draw() {
    
    currentTime = video.currentTime;

    $slideMaster.slider("setValue", currentTime);
    

    // Pinta el video en el canvas
    context.drawImage(video, 0, 0, video.width, video.height);

    let element;

    // Filto los elementos que se deben visualizar
    let elements = data.filter(function (element) {
      return currentTime >= element.start && currentTime <= element.end;      
    });

    
    // Reprent
    $.each(elements, function (index, element) {

      switch(element.type) {
        case "text":
          textRender(element);
          break;

        case "image":
          imageRender(element);
          break;
      }

    })

  }

  /**
   * Representa un elemento de tipo texto
   * @param {*} element 
   */
  function textRender(element) {
    context.fillStyle = element.color;
    context.font = element.size + "px " + element.font;

    if (element.width > 0) {
      let lines = getLines(element.text, element.width);
      let y = element.y + element.size;
      
      for (let i = 0; i < lines.length; i++) {
        context.fillText(lines[i], element.x, y);
        y += element.size;

        if (element.height > 0 && y > element.y + element.height) {
          break;
        }
      }
      
    }
    else {
      context.fillText(element.text, element.x, element.y + element.size);
    }
  }

  /**
   * Representa un elemento de tipo imagen
   * @param {*} element 
   */
  function imageRender(element) {
    let image = document.getElementById("img-" + element.image);
    context.drawImage(image, element.x, element.y, element.width, element.height);


  }

  /**
   * Divide un texto en lineas segun un acho fijo
   * @param {*} text 
   * @param {*} width 
   * @returns 
   */
  function getLines(text, width) {
    let lines = [];
    let words = text.split(" ");
    let line = words[0];

    for (let i = 1; i < words.length; i++) {

      let word = words[i];
      let lineWidth = context.measureText(line + ' ' + word).width;

      if (lineWidth < width) {
        line += ' ' + word;
      }
      else {
        lines.push(line);
        line = word;
      }

    }

    lines.push(line);


    return lines;
    
  }

  function elementAdd(type) {

    let ids = data.map(function (element) {
      return element.id;
    });
    let id = 1;

    if (ids.length)
      id = Math.max(...ids) + 1

    let element = elementDefaults[type];
    element.end = video.duration;
    element.id = id;
    element.name = "Track " + element.id;
    data.push(element);
    trackAdd(element);
    $("#track-" + element.id + " .edit-btn").click();

    $tracksContent.scrollTop($tracksContent[0].scrollHeight);
  }

  function elementDelete(element) {
    var i = data.indexOf(element);
 
    if ( i !== -1 ) {
      data.splice( i, 1 );
    }
  }

  function elementSave() {
    let form = $propertiesForm.get(0);
    let formData = new FormData(form);

    let ids = data.map(function (element) {
      return element.id;
    });

    let key = ids.indexOf(parseInt(formData.get('id')));
    let element = data[key];
    let isNumeric = ['width', 'height', 'x', 'y', 'size', 'id']

    for(var field of formData.entries()) {
      if (isNumeric.indexOf(field[0]) !== -1) {
        element[field[0]] = parseInt(field[1]);
      } 
      else {
        if (field[0] === "name") {
          $("#track-" + element.id + " .text").html(field[1]);
        }

        element[field[0]] = field[1];
      }
      
    }

    data[key] = element;

    form.reset();

    $properties.removeClass("selected");
    $("#track-" + element.id).removeClass("selected");

  }

  function videoSave() {
    storage.setItem("data", JSON.stringify(data));

  }

  $tracksContent.sortable({
    update: function( event, ui ){
     let order = [];
     
     $tracksContent.find('.track').each(function () {
       let $this = $(this);
       let id = parseInt($this.data('id'));
       let elements = data.filter(function(element){
         return element.id == id;

       });

       if (elements.length > 0) {
         order.push(elements[0]);
       }
     });
     data = order;
    }
  });

  //#endregion

})